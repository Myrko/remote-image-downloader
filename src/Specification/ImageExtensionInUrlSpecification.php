<?php

namespace RemoteImageDownloader\Specification;

use RemoteImageDownloader\Downloader\DownloaderResponseInterface;

/**
 * Class ImageSpecification
 * @package RemoteImageDownloader\Specification
 */
class ImageExtensionInUrlSpecification implements ImageSpecificationInterface
{
    /**
     * @var array
     */
    private $allowedExtensions;

    /**
     * ImageSpecification constructor.
     *
     * @param array $allowedExtensions
     */
    public function __construct(array $allowedExtensions)
    {
        $this->allowedExtensions = $allowedExtensions;
    }

    /**
     * @param DownloaderResponseInterface $downloaderResponse
     *
     * @return bool
     */
    public function isSatisfiedBy(DownloaderResponseInterface $downloaderResponse): bool
    {
       $info = pathinfo($downloaderResponse->getUrl());

       return in_array($info['extension'], $this->allowedExtensions);
    }
}