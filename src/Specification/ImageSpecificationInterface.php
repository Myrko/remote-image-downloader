<?php

namespace RemoteImageDownloader\Specification;

use RemoteImageDownloader\Downloader\DownloaderResponseInterface;

/**
 * Interface ImageSpecificationInterface
 * @package RemoteImageDownloader
 */
interface ImageSpecificationInterface
{
    public function isSatisfiedBy(DownloaderResponseInterface $downloaderResponse): bool;
}