<?php

namespace RemoteImageDownloader;

use RemoteImageDownloader\Downloader\DownloaderInterface;
use RemoteImageDownloader\Downloader\DownloaderResponseInterface;
use RemoteImageDownloader\Exception\FileNotSatisfiesSpecificationException;
use RemoteImageDownloader\Filesystem\FilenameGeneratorInterface;
use RemoteImageDownloader\Filesystem\FilesystemInterface;
use RemoteImageDownloader\Specification\ImageSpecificationInterface;

/**
 * Class RemoteImageDownloader
 * @package RemoteImageDownloader
 */
class RemoteImageDownloader
{
    /**
     * @var DownloaderInterface
     */
    private $downloader;

    /**
     * @var FilesystemInterface
     */
    private $filesystem;

    /**
     * @var ImageSpecificationInterface
     */
    private $imageSpecification;

    /**
     * @var FilenameGeneratorInterface
     */
    private $filenameGenerator;

    /**
     * RemoteImageDownloader constructor.
     *
     * @param DownloaderInterface         $downloader
     * @param FilesystemInterface         $filesystem
     * @param ImageSpecificationInterface $imageSpecification
     */
    public function __construct(
        DownloaderInterface $downloader,
        FilesystemInterface $filesystem,
        ImageSpecificationInterface $imageSpecification,
        FilenameGeneratorInterface $filenameGenerator,
        string $path
    ) {
        $this->downloader = $downloader;
        $this->filesystem = $filesystem;
        $this->imageSpecification = $imageSpecification;
        $this->filenameGenerator = $filenameGenerator;
    }

    /**
     * @param string $url
     *
     * @throws FileNotSatisfiesSpecificationException
     */
    public function download(string $url, string $path)
    {
        $response = $this->downloader->download($url);
        if (!$this->imageSpecification->isSatisfiedBy($response)) {
            throw new FileNotSatisfiesSpecificationException(
                'File does not saisfies specification pattern class provided in ' . get_class($this->imageSpecification)
            );
        }

        $this->filesystem->write(
            $this->getDownloadedFileSavePath($response, $path),
            $response->getContent()
        );
    }

    /**
     * @param DownloaderResponseInterface $downloaderResponse
     *
     * @return string
     */
    private function getDownloadedFileSavePath(DownloaderResponseInterface $downloaderResponse, string $path)
    {
        return $path . DIRECTORY_SEPARATOR . $this->filenameGenerator->generateFilename($downloaderResponse);
    }
}