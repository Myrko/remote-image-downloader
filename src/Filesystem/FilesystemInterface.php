<?php

namespace RemoteImageDownloader\Filesystem;

/**
 * Interface FilesystemInterface
 * @package RemoteImageDownloader\Filesystem
 */
interface FilesystemInterface
{
    /**
     * @param string $key
     *
     * @return bool
     */
    public function has(string $key): bool;

    /**
     * @param string $key
     * @param string $content
     * @param bool   $overwrite
     *
     * @return mixed
     */
    public function write(string $key, string $content, bool $overwrite = false): bool;

    /**
     * @param string $key
     *
     * @return string
     */
    public function read(string $key): string;

    /**
     * @param string $key
     *
     * @return bool
     */
    public function delete(string $key): bool;
}