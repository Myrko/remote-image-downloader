<?php

namespace RemoteImageDownloader\Filesystem;

use RemoteImageDownloader\Downloader\DownloaderResponseInterface;

/**
 * Class OriginalFilenameGenerator
 * @package RemoteImageDownloader\Filesystem
 */
class OriginalFilenameGenerator implements FilenameGeneratorInterface
{
    /**
     * @param DownloaderResponseInterface $downloaderResponse
     *
     * @return string
     */
    public function generateFilename(DownloaderResponseInterface $downloaderResponse): string
    {
        $info = pathinfo($downloaderResponse->getUrl());

        return $info['filename'];
    }
}