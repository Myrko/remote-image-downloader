<?php

namespace RemoteImageDownloader\Filesystem;

use RemoteImageDownloader\Downloader\DownloaderResponseInterface;

/**
 * Interface FilenameGeneratorInterface
 * @package RemoteImageDownloader\Filesystem
 */
interface FilenameGeneratorInterface
{
    /**
     * @param DownloaderResponseInterface $downloaderResponse
     *
     * @return string
     */
    public function generateFilename(DownloaderResponseInterface $downloaderResponse): string;
}