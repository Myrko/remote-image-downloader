<?php

namespace RemoteImageDownloader\Downloader;

/**
 * Interface DownloaderInterface
 * @package RemoteImageDownloader\Downloader
 */
interface DownloaderInterface
{
    /**
     * @param string $url
     *
     * @return DownloaderResponseInterface
     */
    public function download(string $url): DownloaderResponseInterface;
}