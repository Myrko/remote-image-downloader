<?php

namespace RemoteImageDownloader\Downloader;

/**
 * Class FileGetContentsDownloader
 * @package RemoteImageDownloader\Downloader
 */
class FileGetContentsDownloader implements DownloaderInterface
{
    /**
     * @param string $url
     *
     * @return DownloaderResponseInterface
     */
    public function download(string $url): DownloaderResponseInterface
    {
        $content = file_get_contents($url);

        return new DownloaderResponse([], $content, $url);
    }
}