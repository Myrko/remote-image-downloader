<?php

namespace RemoteImageDownloader\Downloader;

/**
 * Class DownloaderResponse
 * @package RemoteImageDownloader\Downloader
 */
class DownloaderResponse implements DownloaderResponseInterface
{
    /**
     * @var array
     */
    private $headers;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $url;

    /**
     * DownloaderResponse constructor.
     *
     * @param array  $headers
     * @param string $content
     * @param string $url
     */
    public function __construct(array $headers, string $content, string $url)
    {
        $this->headers = $headers;
        $this->content = $content;
        $this->url = $url;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
       return $this->content;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}
