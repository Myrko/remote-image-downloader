<?php

namespace RemoteImageDownloader\Downloader;

/**
 * Interface DownloaderResponseInterface
 * @package RemoteImageDownloader\Downloader
 */
interface DownloaderResponseInterface
{
    /**
     * DownloaderResponseInterface constructor.
     *
     * @param array  $headers
     * @param string $content
     */
    public function __construct(array $headers, string $content, string $url);

    /**
     * @return array
     */
    public function getHeaders(): array;

    /**
     * @return string
     */
    public function getContent(): string;

    /**
     * @return string
     */
    public function getUrl(): string;
}